package nl.bioinf.java_wrapper;

public class MessagingController {
    private final OptionsProvider optionsProvider;

    /**
     * the constructor need an OptionsProvider to be able to do its work.
     * @param optionsPovider the options provider
     */
    public MessagingController(final OptionsProvider optionsPovider) {
        this.optionsProvider = optionsPovider;
    }

    /**
     * Starts the application.
     */
    public void start() {
        String origdata = optionsProvider.getOrigData();
        String model = optionsProvider.getModel();
        String newdata = optionsProvider.getNewData();

    }
}