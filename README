				Wrapper of model created for project:
				Machine learning model for detecting cervical cancer
				Author: Olle de Jong (Michiel Noback)

The purpose of this project was creating a Java Wrapper for the main project which you can find here: 
https://bitbucket.org/olledejong/theme09projectodj/src/master/
This wrapper was initially made for specific usage to predict if a person was suffering cervical cancer or not.
But, because all parameters are variable you can also use this wrapper for other purposes.

The wrapper was created to be able to classify new instances more easily.

[PREREQUISITES]
- IntelliJ IDEA
- SDK: Java version 10.0.2
- JDK 10.0.2
- Gradle (built into IntelliJ IDEA)

[USAGE] This wrapper can be run using IntelliJ IDEA:
1) Clone or fork this repository (top right of your browser window)
2) Open the project in IntelliJ IDEA
3) Run the Runner.java with the following command line arguments:

 -f,--origdata <arg>   Exact path to the modified input data which is already cassified
	EXAMPLE : data/mod_risk_factors_cervical.arff

 -m,--model <arg>      Exact path to the input model file that will be used to classify new data
	EXAMPLE : data/j48.model

 -u,--newdata <arg>    Exact path to the unknown (yet to be classified) instances file you want to classify
	EXAMPLE : data/unknowndataset.arff

 -h,--help             Prints this help message

4) Look and analyse the output in the console.

