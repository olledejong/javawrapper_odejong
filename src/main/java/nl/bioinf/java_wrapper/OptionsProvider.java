package nl.bioinf.java_wrapper;

/**
 * interface that specifies which options should be provided to the tool.
 */
public interface OptionsProvider {
    /**
     * serves the path to the file with original data.
     * @return origdata
     */
    String getOrigData();
    /**
     * serves the model file.
     * @return model
     */
    String getModel();
    /**
     * serves the age of the application user.
     * @return newdata
     */
    String getNewData();
}
