package nl.bioinf.java_wrapper;

import weka.classifiers.trees.J48;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import java.io.IOException;
import java.io.File;
/**
 * The WekaRunner class, when called, runs WEKA with the provided parameters
 */
public class WekaRunner {

    private final OptionsProvider optionsProvider;


    public WekaRunner(OptionsProvider optionsProvider){
        this.optionsProvider = optionsProvider;
    }
    /**
     * The start method calls all other methods so WEKA is run.
     */
    public void start() {
        String datafile = optionsProvider.getOrigData();
        String unknownFile = optionsProvider.getNewData();
        String modelFile = optionsProvider.getModel();
        //check if path to modelfile is a legit
        File modelFile2 = new File(modelFile);
        File unknownFile2 = new File(unknownFile);
        File datafile2 = new File(datafile);
        if(!modelFile2.exists()) {
            throw new IllegalArgumentException("This path: " + modelFile2 + " for the model file does not exist.");
        } else if(!unknownFile2.exists()) {
            throw new IllegalArgumentException("This path: " + unknownFile2 + " for the unknown data file does not exist");
        } else if(!datafile2.exists()) {
            throw new IllegalArgumentException("This path: " + datafile2 + " for the (training) data file does not exist");
        }

        try {
            Instances instances = loadArff(datafile);
            printInstances(instances);
            J48 j48 = buildClassifier(instances);
            saveClassifier(j48, modelFile);
            J48 fromFile = loadClassifier(modelFile);
            Instances unknownInstances = loadArff(unknownFile);
            System.out.println("\nunclassified unknownInstances = \n" + unknownInstances);
            classifyNewInstance(fromFile, unknownInstances);

        } catch (Exception e) {
            System.out.println(e);
        }
    }
    /**
     * This method classifies new instances using the model
     */
    private void classifyNewInstance(J48 tree, Instances unknownInstances) throws Exception {
        // create copy
        Instances labeled = new Instances(unknownInstances);
        // label instances
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            double clsLabel = tree.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        System.out.println("\nNew, labeled = \n" + labeled);
    }
    /**
     * This method load the classifier
     */
    private J48 loadClassifier(String modelFile) throws Exception {
        // deserialize model
        return (J48) weka.core.SerializationHelper.read(modelFile);
    }
    /**
     * This method saves the built classifier and calls WEKA
     */
    private void saveClassifier(J48 j48, String modelFile) throws Exception {
        //post 3.5.5
        // serialize model
        weka.core.SerializationHelper.write(modelFile, j48);

        // serialize model pre 3.5.5
//        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(modelFile));
//        oos.writeObject(j48);
//        oos.flush();
//        oos.close();
    }
    /**
     * This method builds the classifier and returns it
     */
    private J48 buildClassifier(Instances instances) throws Exception {
        String[] options = new String[1];
        options[0] = "-U";            // unpruned tree
        J48 tree = new J48();         // new instance of tree
        tree.setOptions(options);     // set the options
        tree.buildClassifier(instances);   // build classifier
        return tree;
    }
    /**
     * This method prits the instances in the data files
     */
    private void printInstances(Instances instances) {
        int numAttributes = instances.numAttributes();

        for (int i = 0; i < numAttributes; i++) {
            System.out.println("attribute " + i + " = " + instances.attribute(i));
        }

        System.out.println("class index = " + instances.classIndex());
//        Enumeration<Instance> instanceEnumeration = instances.enumerateInstances();
//        while (instanceEnumeration.hasMoreElements()) {
//            Instance instance = instanceEnumeration.nextElement();
//            System.out.println("instance " + instance. + " = " + instance);
//        }

        //or
        int numInstances = instances.numInstances();
        for (int i = 0; i < numInstances; i++) {
            if (i == 5) break;
            Instance instance = instances.instance(i);
            System.out.println("instance = " + instance);
        }
    }
    /**
     * This method loads the ARFF file commandline argument
     * @param datafile Known datafile
     */
    private Instances loadArff(String datafile) throws IOException {
        try {
            ConverterUtils.DataSource source = new ConverterUtils.DataSource(datafile);
            Instances data = source.getDataSet();
            // setting class attribute if the data format does not provide this information
            // For example, the XRFF format saves the class attribute information as well
            if (data.classIndex() == -1)
                data.setClassIndex(data.numAttributes() - 1);
            return data;
        } catch (Exception e) {
            throw new IOException("could not read from file");
        }
    }
}

