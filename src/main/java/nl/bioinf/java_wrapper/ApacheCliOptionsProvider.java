package nl.bioinf.java_wrapper;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * This class implements the OptionsProvider interface by parsing the passed command line arguments.
 *
 * @author michiel
 */
public class ApacheCliOptionsProvider implements OptionsProvider {
    private static final String HELP = "help";
    private static final String ORIGDATA = "origdata";
    private static final String MODEL = "model";
    private static final String NEWDATA = "newdata";

    private final String[] clArguments;
    private Options options;
    private CommandLine commandLine;

    /**
     * constructs with the command line array.
     *
     * @param args the CL array
     */
    public ApacheCliOptionsProvider(final String[] args) {
        this.clArguments = args;
        initialize();
    }

    /**
     * Options initialization and processing.
     */
    private void initialize() {
        buildOptions();
        processCommandLine();
        //System.out.println(origdata);
    }

    /**
     * check if help was requested; if so, return true.
     * @return helpRequested
     */
    public boolean helpRequested() {
        return this.commandLine.hasOption(HELP);
    }

    /**
     * builds the Options object
     */
    private void buildOptions() {
        // create Options object
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "Prints this message");
        Option originaldataOption = new Option("f", ORIGDATA, true, "The modified input data which is already cassified");
        Option modelOption = new Option("m", MODEL, true, "Input model file that will be used to classify new data");
        Option newdataOption = new Option("u", NEWDATA, true, "Unclassified new data input file");
        options.addOption(helpOption);
        options.addOption(originaldataOption);
        options.addOption(modelOption);
        options.addOption(newdataOption);
    }


    private void processCommandLine() {
        try {
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.clArguments);

        } catch (ParseException ex) {
            throw new IllegalArgumentException(ex);
        }
    }
    /**
     * prints help when needed
     */
    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("MyCoolTool", options);
    }

    @Override
    public String getOrigData() {
        return this.commandLine.getOptionValue(ORIGDATA);
    }

    @Override
    public String getModel() {
        return this.commandLine.getOptionValue(MODEL);
    }

    @Override
    public String getNewData() {
        return this.commandLine.getOptionValue(NEWDATA);
    }

}